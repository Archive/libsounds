2008-07-25  Rob Bradford  <rob@robster.org.uk>

	* sound-properties.h:
	Don't include the individual gtk/gtkobject.h header file. Use
	gtk/gtk.h instead.
	* sound-view.c:
	Remove the inclusion of the unused libgnomeui header file.

2008-02-12  Kjartan Maraas  <kmaraas@gnome.org>

	* Makefile.am:
	* sound-view.c:
	Fix build with Gio. Patch from Luca Ferreti. Closes bug #515956.

2008-02-11  Rodrigo Moya <rodrigo@gnome-db.org>

	* sound-view.c (combo_box_changed_cb): replace GnomeVFS usage with
	GIO equivalent.

2008-02-05  Jens Granseuer  <jensgr@gmx.net>

	* Makefile.am: use libtool to generate the library so we also get a
	non-static version which is needed by gnome-settings-daemon

2007-01-14  Thomas Wood  <thos@gnome.org>

	Based on patch by: Sylvain Defresne

	* sound-view.c: (combo_box_changed_cb), (add_sound_item),
	(sound_view_init): Set the play button to insensitive if "No sound" is
	selected. Fixes bug 353828.

2007-01-06  Gabor Kelemen  <kelemeng@gnome.hu>

	Fixes #371747 

	* sound-view.c: marked "Select sound file" translatable.

2006-03-03  Rodrigo Moya <rodrigo@novell.com>

	Fixes #333125

	* sound-view.c (sound_view_init, create_populate_combo_box): mark
	missing strings for translation.

2006-02-27  Rodrigo Moya <rodrigo@novell.com>

	Fixes #332530

	* sound-event.c (sound_event_free): free also event->desc.

2006-01-23  Jens Granseuer <jensgr@gmx.net>

	Fixes #328255

	* sound-view.c (combo_box_changed_cb, add_sound_item): removed
	C99-isms.

2006-01-19  Damien Carbery <damien.carbery@sun.com>

	* sound-view.c (play_preview_cb): don't return values on void
	function.

2006-01-19  Rodrigo Moya <rodrigo@novell.com>

	* sound-view.c (sound_view_init): reduced the size of the dialog
	a little bit.

2006-01-19  Dennis Cranston <dennis_cranston@yahoo.com>

	* sound-view.c (add_sound_item): use GTK_STOCK_MEDIA_PLAY icon for
	play buttons.

2006-01-11  Rodrigo Moya <rodrigo@novell.com>

	Fixes #326635

	* sound-view.c (play_preview_cb ): added missing space to string.

2006-01-11  Scott Reeves <SReeves@novell.com>

	* sound-event.[ch] (sound_event_free, sound_event_set_oldfile):
	added code to manage new field in SoundEvent structure.

	* sound-properties.c (sound_properties_add_event): ditto.
	(sound_properties_add_file): we only do system events now.

	* sound-view.c: changed to match new GUI.

2004-10-28  Sebastien Bacher  <seb128@debian.org>

	* sound-view.c: (remove_cb), (sound_view_init), (select_row_cb):
	patch by June Tate <june@theonelab.com> to add a "remove" button that 
	allow to turn off a sound event (Closes: #14272).

2004-10-14  Jody Goldberg <jody@gnome.org>

	* Release 2.8.1

2004-04-15  Jody Goldberg <jody@gnome.org>

	* Release 2.6.1

2004-04-01  Jody Goldberg <jody@gnome.org>

	* Release 2.6.0.3

2004-03-30  Jody Goldberg <jody@gnome.org>

	* Release 2.6.0.1

2004-03-23  Jody Goldberg <jody@gnome.org>

	* Release 2.6.0

2004-03-11  Jody Goldberg <jody@gnome.org>

	* Release 2.5.4

2004-03-02  Fernando Herrera  <fherrera@onirica.com>

	* sound-view.c: (sound_view_init): Use GtkFileChooser in the sound
	entry. Fix #135923

2004-02-13  Jody Goldberg <jody@gnome.org>

	* Release 2.5.3

2004-02-05  Shakti Sen  <shakti.sen@wipro.com>

	* sound-view.c: (play_cb): Show error message, when playing a 
	nonexistent file. Fixes bug #121263

2004-01-16  Padraig O'Briain <padraig.obriain@sun.com>

	* sound-view.c: (sound_view_init): Update mnemonic widget to be
	a GtkCombo instead of its contained GtkEntry. Fixes bug #126972.

2004-01-14  Jody Goldberg <jody@gnome.org>

	* Release 2.5.2

2003-12-30  Jody Goldberg <jody@gnome.org>

	* Release 2.5.1.1

2003-12-30  Jody Goldberg <jody@gnome.org>

	* Release 2.5.1

2003-10-28  Jody Goldberg <jody@gnome.org>

	* Release 2.5.0

2003-07-31  Balamurali Viswanathan <balamurali.viswanathan@wipro.com>

	* sound-view.c (entry_changed_cb): Give a error message if the
	user associates an invalid wav file for an event. Bug #109529

2003-07-11  Dennis Cranston <dennis_cranston at yahoo com>

	* sound-view.c:  Fixes for ui-review bug number 99533.
	  - s/File to play/Sound File,
	  - HIG Frame stuff,
	  - Play button looks better to the right of the Browse button,
	  - Columns aren't resizable,
	  - Can't click on the column headers to sort them,
	  - Text entry should have label "Sound File" and mnemonic - above
	    text entry to allow field to be wider.
	
2003-07-07  Jody Goldberg <jody@gnome.org>

	* Release 2.3.4

2003-06-24  Jody Goldberg <jody@gnome.org>

	* Release 2.3.3

Fri Jun  6 19:34:32 2003  Jonathan Blandford  <jrb@gnome.org>

	* sound-view.c (compare_func): don't ever call g_ascii_strcasecmp
	with NULL as an argument.

2003-05-07  Jody Goldberg <jody@gnome.org>

	* Release 2.3.1

Tue Feb  4 17:09:18 2003  Jonathan Blandford  <jrb@redhat.com>

	* Release 2.2.0.1

2003-01-10  Jody Goldberg <jody@gnome.org>

	* Release 2.1.6

2002-12-18  Jody Goldberg <jody@gnome.org>

	* Release 2.1.5

2002-11-23  Jody Goldberg <jody@gnome.org>

	* Release 2.1.3

2002-11-02  Jody Goldberg <jody@gnome.org>

	* Release 2.1.2

2002-10-21  Jody Goldberg <jody@gnome.org>

	* Release 2.1.1

2002-10-01  Jody Goldberg <jody@gnome.org>

	* Release 2.1.0.1

2002-09-09  Jody Goldberg <jody@gnome.org>

	http://bugzilla.gnome.org/show_bug.cgi?id=76437
	* sound-view.c (foreach_cb) : dunno why this doesn't work, but it
	  doesn't and I'm unwilling to waste much time tracking down why.

	* sound-view.c (sound_view_init) : 90936 patch from padraig

2002-08-21  Jody Goldberg <jody@gnome.org>

	* Release 2.1.0

2002-07-31  Federico Mena Quintero  <federico@ximian.com>

	* sound-view.c (sound_view_init): Make the file entry modal.

2002-06-18  jacob berkman  <jacob@ximian.com>

	* sound-properties.c (sound_properties_add_defaults): don't have
	// in the file name

2002-06-17  Jody Goldberg <jody@gnome.org>

	* Release 2.0.0

2002-05-07  Jody Goldberg <jody@gnome.org>

	* sound-event.c (sound_event_free) : rename from sound_event_unref.
	(sound_event_ref) : delete.  There is no need to ref count this.

	* sound-properties.c (sound_properties_destroy) : handle multiple calls
	(sound_properties_remove_event) : use sound_event_free.

2002-04-27  Jody Goldberg <jody@gnome.org>

	* sound-view.c (sound_view_destroy) : Handle multiple calls.

2002-04-10  Kjartan Maraas  <kmaraas@gnome.org>

	* sound-view.c (sound_view_init): Mark a string for translation.
	
2002-03-28  jacob berkman  <jacob@ximian.com>

	* sound-properties.c (signals): this should be static

2002-03-27  Richard Hestilow  <hestilow@ximian.com>

	* sound-properties.c (sound_properties_add_file): Filter out
	gnome and gtk-events 1.x events, since they're duplicates.
	(sound_properties_save): Propogate some changes to the GNOME1
	duplicates.

	* sound-view.c (compare_func): Move gnome check elsewhere.
	(sound_view_init): set sort_func, not default_sort_func (seems
	to make it work), and add mnemonics.
	(foreach_cb): Don't add the subtree unless it has children.

2002-03-06  Richard Hestilow  <hestilow@ximian.com>

	* sound-view.c, sound-properties.c: Remove debugging g_prints.

2002-03-05  Richard Hestilow  <hestilow@ximian.com>

	* sound-view.c (entry_changed_cb): Remove double semicolons.

2002-03-05  Richard Hestilow  <hestilow@ximian.com>

	* sound-view.c: Lauris rewrote this to use TreeView.

2002-02-27  Kjartan Maraas  <kmaraas@gnome.org>

	* sound-view.c: #include <config.h> at the top.
	
2002-02-12  Lauris Kaplinski  <lauris@ximian.com>

	* sound-view.c (play_cb): Destroy dialog here

	* sound-properties.c (sound_properties_get_type): Use new glib syntax
	(sound_properties_new): Use g_object_new
	(sound_properties_thaw): Use g_signal_emit
	(sound_properties_add_directory): Use g_build_filename
	(sound_properties_add_event): Ditto
	(sound_properties_add_defaults): Use gnome_program_locate_file
	(sound_properties_event_changed): Use g_signal_emit
	(sound_properties_changed): Ditto
	(sound_properties_remove_event): Ditto

	* sound-view.c (play_cb): Use g_file_test, use gnome_program_locate_file
	(sound_view_init): Use GNOME_STOCK_VOLUME, gnome_program_locate_file
	(sound_view_reload): Kill warning

	* sound-properties.c (sound_properties_class_init): Use g_signal_new

2002-02-04  Lauris Kaplinski  <lauris@ximian.com>

	* sound-view.c (compare_func): Replace deprecated method
	(play_cb): Use gtk_message_dialog methods
	(entry_changed_cb): Kill warning
	(sound_view_init): Use g_signal_connect directly
	(sound_view_new): Ditto
	(select_row_cb): Replace deprecated methods

	* sound-properties.c (strip_ext): Replace deprecated method
	(sound_properties_add_file): Ditto

2002-01-07  Richard Hestilow  <hestilow@ximian.com>

	* sound-view.c (sound_properties_class_init):
	s/gtk_marshal_NONE__OBJECT/gtk_marshal_NONE__POINTER/g

	* sound-view.c (sound_view_init): Set hint on "play"
	button to not trigger gtk+ sound event.

2002-01-04  Richard Hestilow  <hestilow@ximian.com>

	* sound-properties.c (sound_properties_class_init): Remove
	call to gtk_object_class_add_signals.

2001-11-18  Richard Hestilow  <hestilow@ximian.com>

	* sound-properties.c (sound_properties_add_defaults): Remove
	nonsensical precondition.

	* sound-view.c (entry_changed_cb, event_changed_cb): Added.
	(sound_view_init): Create a hash table mapping events to row data.
	Also, mess with the clist sizing a little, and connect to the
	changed signals on the file entry and the sound properties.
	(foreach_cb): Insert event into hash table, and set ignore_changed
	to TRUE while updating the file entry.
	(sound_view_destroy): Destroy the hash table.

	* README, ChangeLog: Added.
